﻿using System;

namespace PP0601A2
{
    class Program
    {
        static void Main()
        {
            short counter = 0, old = 42;

            while (counter < 3)
            {
                short a = Int16.Parse(Console.ReadLine());

                if (a == 42 & old != 42)
                {
                    counter++;
                }

                Console.WriteLine(a);
                old = a;
            }
        }
    }
}
