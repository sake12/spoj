﻿using System;
using System.Linq;

namespace POTSAM
{
    class Program
    {
        static void Main()
        {
            string a = Console.ReadLine();

            string[] b = a.Split(' ');

            Console.WriteLine(int.Parse(b[0]) * int.Parse(b[1]) + int.Parse(b[2]) * int.Parse(b[3]));
        }
    }
}

