﻿using System;
using System.Linq;

namespace JPESEL
{
    class Program
    {
        static void Main()
        {
            int testy = int.Parse(Console.ReadLine());
            
            for (int i = 0; i < testy; i++)
            {
                var b = Console.ReadLine().ToArray();
                int wynik = 0;
                wynik = wynik + Int32.Parse(b[0].ToString());
                wynik = wynik + Int32.Parse(b[1].ToString()) * 3;
                wynik = wynik + Int32.Parse(b[2].ToString()) * 7;
                wynik = wynik + Int32.Parse(b[3].ToString()) * 9;
                wynik = wynik + Int32.Parse(b[4].ToString());
                wynik = wynik + Int32.Parse(b[5].ToString()) * 3;
                wynik = wynik + Int32.Parse(b[6].ToString()) * 7;
                wynik = wynik + Int32.Parse(b[7].ToString()) * 9;
                wynik = wynik + Int32.Parse(b[8].ToString());
                wynik = wynik + Int32.Parse(b[9].ToString()) * 3;
                wynik = wynik + Int32.Parse(b[10].ToString());

                Console.WriteLine(wynik.ToString().EndsWith("0") ? "D" : "N");
            }
        }
    }
}
